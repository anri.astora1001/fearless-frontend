const form = document.getElementById('login-form');
form.addEventListener('submit', loginFormSend)

async function loginFormSend(event) {
    event.preventDefault();

    const data = new FormData(form)
    console.log(data)
    const fetchOptions = {
      method: 'post',
      body: data,
      credentials: 'include',
      //headers: {
      //  'Content-Type': 'application/json',
      //}
    };
    const url = 'http://localhost:8000/login/';
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      window.location.href = '/';
    } else {
      console.error(response);
    }
}