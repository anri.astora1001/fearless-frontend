const api = 'http://localhost:8000/api/states/'

try{
    const response = await fetch(api)
    if(!response.ok) {
        //bad response handling
    } else {
        const data = await response.json()
        //console.log(data)

        //const selectTag = document.getElementById('state')
        
        data.states.forEach((state) => {
            //console.log(state)

            const name = state.name
            const abbrev = state.abbreviation

            const html = createOption(name, abbrev)
            const options = document.querySelector('.form-select')
            options.innerHTML += html
        })
    }} catch(e) {
        // error handling
    }

const form = document.getElementById('create-location-form')
form.addEventListener('submit', formSubmit)


async function formSubmit(event){
    event.preventDefault()
    const formData = new FormData(form)
    let formObj = Object.fromEntries(formData.entries())
    formObj.state = formObj.state.split(":")[0].trim()
    console.log(formObj)
    const formJSON = JSON.stringify(formObj)
    sendForm(formJSON, form)

}

async function sendForm(json, formTag){
    //console.log(json)
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
            },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
    }
}

function createOption(name, abbrev){
    return `<option>${abbrev} : ${name}</option>`
}