const selectTag = document.getElementById('conference');

const url = 'http://localhost:8000/api/conferences/';
const response = await fetch(url);
if (response.ok) {
  const data = await response.json();

  for (let conference of data.conferences) {
    const option = document.createElement('option');
    option.value = conference.href;
    option.innerHTML = conference.name;
    selectTag.appendChild(option);
  }
  const loadingIcon = document.getElementById('blinking-bs')
  //console.log(loadingIcon.classList)
  loadingIcon.classList.add('d-none')

  const selector = document.getElementById('conference')
  selector.classList.remove('d-none')
}
const form = document.getElementById('create-attendee-form')
form.addEventListener('submit', formSubmit)

async function formSubmit(event){
    event.preventDefault()
    const formData = new FormData(form)
    const formObj = Object.fromEntries(formData.entries())
    //console.log(formObj)
    const conferenceId = formObj["conference"].split("/")[3]
    //console.log(conferenceId)
    const formJSON = JSON.stringify(formObj)
    //console.log(formJSON)
    sendForm(formJSON, form, conferenceId)
}

async function sendForm(json, formTag, id){
    //console.log(json)
    const attendeeUrl = `http://localhost:8001/api/conferences/${id}/attendees/`;
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
            },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newAttendee = await response.json();
        console.log(newAttendee)
    }
}