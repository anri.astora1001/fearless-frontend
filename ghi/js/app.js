function createCol(name, location, description, pictureUrl, date) {
    //console.log(`name: ${name} description: ${description} pictureUrl: ${pictureUrl}`)
    return `
    <div class="col">
      <div class="card shadow w-100">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text text-muted">${location}</p>
          <p class="card-text">${description}</p>
          <p class="card-footer">${date}</p>
        </div>
      </div>
    </div>
    `
  }

const url = 'http://localhost:8000/api/conferences/'
const cookie = cookieStore.get("jwt_access_payload")
if (cookie){
  console.log(cookie)

} else {
  console.log("cookie not found")
}

try{
    const response = await fetch(url)
    if(!response.ok) {
        //bad response handling
    } else {
        const data = await response.json()

        data.conferences.forEach(async conference =>{
            const detailUrl = `http://localhost:8000${conference.href}`
            const detailResponse = await fetch(detailUrl)
            

            if (detailResponse.ok) {
                const details = await detailResponse.json()
                const title = details.conference.name
                const location = details.conference.location.name
                const description = details.conference.description
                const pictureUrl = details.conference.location.picture_url
                
                const dateStart = new Date(details.conference.starts)
                const dateEnd = new Date(details.conference.ends)
                const date = dateStart.toLocaleString() + " - " + dateEnd.toLocaleString() // 5/12/2020, 6:50:21 PM

                const html = createCol(title, location, description, pictureUrl, date)
                const column = document.querySelector('.row');
                column.innerHTML += html;
                //console.log(details)
            }
        })

        // const conference = data.conferences[0]

        // //header logic
        // const nameTag = document.querySelector('.card-title')
        // nameTag.innerHTML = conference.name

        // //detail logic
        // const detailUrl = `http://localhost:8000${conference.href}`
        // const detailResponse = await fetch(detailUrl)
        // if (detailResponse.ok){
        //     const details = await detailResponse.json()
        
        //     //description logic
        //     const descriptionTag = document.querySelector('.card-text')
        //     descriptionTag.innerHTML = details.conference.description

        //     //Image logic
        //     let imageTag = document.querySelector('.card-img-top')
        //     imageTag.src = details.conference.location.picture_url
        //     console.log(details)
        // }

    }} catch (e) {
        // error handling
    }
