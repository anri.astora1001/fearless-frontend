const api = 'http://localhost:8000/api/locations/'

try{
    const response = await fetch(api)
    if(!response.ok) {
        //bad response handling
    } else {
        const data = await response.json()
        console.log(data)
        loadLocationOptions(data)
    }}catch (e){}

const form = document.getElementById('create-conference-form')
form.addEventListener('submit', formSubmit)

async function formSubmit(event){
    event.preventDefault()
    const formData = new FormData(form)
    const formObj = Object.fromEntries(formData.entries())
    //console.log(formObj)
    const formJSON = JSON.stringify(formObj)
    //console.log(formJSON)
    sendForm(formJSON, form)
}

async function sendForm(json, formTag){
    //console.log(json)
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
            },
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference)
    }
}

function loadLocationOptions(data){
    data.locations.forEach(async (location,i) =>{
        //location.
        const LocResponse = await fetch(`http://localhost:8000${location.href}`)
        if(LocResponse.ok){
            const loc = await LocResponse.json()
            console.log(loc)
            const name = loc.name
            const city = loc.city
            const state = loc.state
            const id = loc.href.split("/")[3]
            //console.log(`id: ${id}`)

            const html = createLocationOption(name, city, state,id)
            const options = document.querySelector('.form-select')
            options.innerHTML += html
        }
    })
}
 
function createLocationOption(name, city, state,id){
    return `<option value="${id}">${name}, ${city}, ${state}</option>`
}